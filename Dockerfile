FROM alpine

EXPOSE 80
WORKDIR /src

RUN apk add -U nginx; \
    mkdir -p /run/nginx

RUN mkdir -p /src
COPY . /src/

RUN rm /etc/nginx/conf.d/default.conf; \
    ln -s /src/default.conf /etc/nginx/conf.d/default.conf

CMD ["nginx", "-g", "daemon off;", "-c", "/etc/nginx/nginx.conf"]

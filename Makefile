CI_REGISTRY_IMAGE ?= registry.gitlab.com/tmaczukin-test-projects/test-visibility-support

build:
	docker build --cache-from $(CI_REGISTRY_IMAGE) -t $(CI_REGISTRY_IMAGE) .

run:
	docker run -d --name visibility-support-test -p 127.0.0.1:8888:80 $(CI_REGISTRY_IMAGE)
	x-www-browser http://127.0.0.1:8888/

stop:
	docker rm -f visibility-support-test
